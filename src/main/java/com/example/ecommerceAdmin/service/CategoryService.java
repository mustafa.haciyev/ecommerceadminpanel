package com.example.ecommerceAdmin.service;

import com.example.ecommerceAdmin.dto.CategoryRequestDto;
import com.example.ecommerceAdmin.entity.Category;
import com.example.ecommerceAdmin.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;

import java.util.List;

public interface CategoryService {



    Category editCategory(Category category);

    void deleteCategory(Long id);

//    Page<Category> getRequestFilters(int page, int limit, String categoryName, Sort.Direction sortType);

    Category addCategory(Category category);

    void findAll();

    List<Category> getAllCategories();
}
