package com.example.ecommerceAdmin.service.impl;

import com.example.ecommerceAdmin.dto.CategoryRequestDto;
import com.example.ecommerceAdmin.entity.Category;
import com.example.ecommerceAdmin.entity.Product;
import com.example.ecommerceAdmin.service.CategoryService;
import com.example.ecommerceAdmin.repos.CategoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Primary
@RequiredArgsConstructor
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository categoryRepository;


    @Override
    public Category addCategory(Category category) {
        return categoryRepository.save(category );
    }

    @Override
    public Category editCategory(Category category) {
        // Kategori id'si null değilse ve veritabanında varsa güncelleme yap
        if (category.getId() != null && categoryRepository.existsById(category.getId())) {
            return categoryRepository.save(category);
        }
        return null; // Kategori bulunamadıysa veya id null ise null dön
    }


    @Override
    public void deleteCategory(Long id) {
        categoryRepository.deleteById(id);
    }

//    @Override
//    public void getRequestFilters() {
//        categ

//    }

    private Page<Category> findCategorysByName(int page,int limit,String productName) {
        Pageable pageable = PageRequest.of(page, limit);
        return categoryRepository.findByCategoryNameContainingIgnoreCase(productName, pageable);
    }

    @Override
    public void findAll() {
        categoryRepository.findAll();
    }

    @Override
    public List<Category> getAllCategories() {
        return categoryRepository.findAll();
    }
}
