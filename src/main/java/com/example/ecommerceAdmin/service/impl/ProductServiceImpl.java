package com.example.ecommerceAdmin.service.impl;

import com.example.ecommerceAdmin.entity.Category;
import com.example.ecommerceAdmin.entity.Product;
import com.example.ecommerceAdmin.repos.CategoryRepository;
import com.example.ecommerceAdmin.repos.ProductRepository;
import com.example.ecommerceAdmin.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
@Primary
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private final CategoryRepository categoryRepository;

    @Override
    public Product addProduct(Product product) {
        Category category = product.getCategory();
        if (category != null) {
            if (category.getId() != null) {
                Category existingCategory = categoryRepository.findById(category.getId())
                        .orElseThrow(() -> new IllegalArgumentException("Kategori bulunamadı: " + category.getId()));
                product.setCategory(existingCategory);
            } else if (category.getCategoryName() != null) {
                Category existingCategory = categoryRepository.findByCategoryName(category.getCategoryName());
                product.setCategory(existingCategory);
            } else {
                throw new IllegalArgumentException("Kategori bilgisi eksik");
            }
        } else {
            throw new IllegalArgumentException("Ürün bilgisinde kategori bulunmuyor");
        }

        return productRepository.save(product);
    }
    @Override
    public Product editProduct(Product product) {
        boolean exist = productRepository.existsById(product.getId());
        if(exist){
            return productRepository.save(product);
        }
        return null;
    }

    @Override
    public void deleteProduct(Long id) {
        productRepository.deleteById(id);
    }

    @Override
    public Page<Product> getRequestFilters(int page,int limit,String productName, Sort.Direction sortType) {
        Page<Product> productPage = null;
        if(productName==null && sortType==null){
            productPage = getProductsList(page,limit);
        }
        if(productName!=null && sortType==null ){
            productPage = findProductsByName(page,limit,productName);
        }
        if(productName==null && sortType != null ){
            productPage = getProductsOrderByPrice(page,limit,sortType);
        }
        if(productName!=null && sortType!=null){
            productPage = findProductsByNameAndOrderByPrice(page,limit,productName,sortType);
        }
        return  productPage;
    }

    private Page<Product> getProductsList(int page, int limit) {
        Pageable pageable = PageRequest.of(page, limit);
        return productRepository.findAll(pageable);
    }

    private Page<Product> findProductsByName(int page,int limit,String productName) {
        Pageable pageable = PageRequest.of(page, limit);
        return productRepository.findByNameContainingIgnoreCase(productName, pageable);
    }


    private Page<Product> getProductsOrderByPrice(int page, int limit,Sort.Direction sortType) {
        Sort sort = Sort.by(sortType, "price");
        Pageable pageable = PageRequest.of(page, limit,sort);
        return productRepository.findAll(pageable);
    }

    private Page<Product> findProductsByNameAndOrderByPrice(int page, int limit,
                                                            String productName,
                                                            Sort.Direction sortType) {
        Sort sort = Sort.by(sortType, "price");
        Pageable pageable = PageRequest.of(page, limit,sort);
        return productRepository.findByNameContainingIgnoreCase(productName,pageable);
    }
}
