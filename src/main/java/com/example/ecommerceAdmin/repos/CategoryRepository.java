package com.example.ecommerceAdmin.repos;

import com.example.ecommerceAdmin.entity.Category;
import com.example.ecommerceAdmin.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {
    Page<Category> findByCategoryNameContainingIgnoreCase(String categoryName, Pageable pageable);

    Category findByCategoryName(String categoryName);

}
