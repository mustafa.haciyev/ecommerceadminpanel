package com.example.ecommerceAdmin.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @NotNull(message = "name can not be null")
    String name;

    @NotNull(message = "description can not be null")
    String description;
    @NotNull(message = "image can not be null")
    String image;
    @NotNull(message = "price can not be null")
    double price;

    @ManyToOne
    @NotNull(message = "category can not be null")
    @JoinColumn(name = "category_id")
    Category category;


}
